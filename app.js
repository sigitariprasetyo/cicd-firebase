if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
  require('dotenv').config()
}

const express = require('express')
const app = express()
const PORT = process.env.PORT
const cors = require('cors')
const morgan = require('morgan')
const err = require('./midleware/errHandler')
const routes = require('./routes/index')

app.use(cors())
app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/', routes)
app.use(err)

app.listen(PORT, () => {
  console.log(`Listening on PORT ${PORT}`);
})

module.exports = app
