const chai = require('chai')
const expect = chai.expect
const chaiHttp = require('chai-http')
const app = require('../app.js')

chai.use(chaiHttp)

let body = {
  firstNumb: 5,
  secondNumb: 3
}
let bodyErr = {
  firstNumb: 'a',
  secondNumb: 'b'
  }

describe('calculator', function () {
  describe('addition number', function () {
    it(`Should Success adding number`, function (done) {
      chai.request(app)
        .post('/add')
        .send(body)
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res.body).to.have.key('result')
          expect(res.body.result).to.be.equal(8)
          done()
        })
    })

    it(`Should Error with status 400`, function (done) {
      chai.request(app)
        .post('/add')
        .send(bodyErr)
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(400)
          expect(res.body).to.be.include('Value must be Number or Integer')
          done()
      })
    })
  })

  describe('subtraction number', function () {
    it(`Should Success subtraction number`, function (done) {
      chai.request(app)
        .post('/min')
        .send(body)
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(200)
          expect(res.body).to.have.key('result')
          expect(res.body.result).to.be.equal(2)
          done()
        })
    })

    it(`Should Error with status 400`, function (done) {
      chai.request(app)
        .post('/min')
        .send(bodyErr)
        .end((err, res) => {
          expect(err).to.be.null
          expect(res).to.have.status(400)
          expect(res.body).to.be.include('Value must be Number or Integer')
          done()
        })
    })
  })
})