const router = require('express').Router()
const calculatorController = require('../controllers/calculator')

router.get('/', (req, res, next) => {
  res.status(200).send({ msg: `Welcome in Ci/CD with Firebase!` })
})

router.post('/add', calculatorController.addNumb)
router.post('/min', calculatorController.minNumb)

module.exports = router