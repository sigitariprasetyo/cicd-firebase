class Calculator {
  static addNumb(req, res, next) {
    let { firstNumb, secondNumb } = req.body

    if (typeof(firstNumb) !== "number" || typeof(secondNumb) !== 'number') {
      next({status: 400, msg: `Value must be Number or Integer`})
    } else {
      let result = Number(firstNumb) + Number(secondNumb)
      res.status(200).json({result})
    }
  }

  static minNumb(req, res, next) {
    let { firstNumb, secondNumb } = req.body

    if (typeof(firstNumb) !== "number" || typeof(secondNumb) !== 'number') {
      next({ status: 400, msg: `Value must be Number or Integer` })
    } else {
      let result = Number(firstNumb) - Number(secondNumb)
      res.status(200).json({ result })
    }
}
}

module.exports = Calculator